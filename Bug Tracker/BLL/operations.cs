﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using DAL;

namespace BLL
{
    public class operations
    {
        public dbconnection db = new dbconnection();

        public DataTable login(string username, string password,string usertype)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from tbl_user where username='" + username + "' and password='" + password + "' and usertype='"+usertype+"'";
            return db.ExeReader(cmd);
        }

        public int AddProjects(string projectname,string projectdescription,string version)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into tbl_project (projectid,project_name,project_desc,version) values (project.nextval,'" + projectname + "','" + projectdescription + "','" + version+"')";
            return db.ExNonQuery(cmd);
        }
        public DataTable getAllProjects()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from tbl_project";
            return db.ExeReader(cmd);
        }
        public int AddUsers(string firstname, string lastname, string address, string phonenumber,string username, string password, string usertype)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into tbl_user (userid,first_name,last_name,address,phonenumber,username,password,usertype) values (userid.nextval,'" + firstname + "','" + lastname + "','" + address + "','"+phonenumber+"','"+username+"','"+password+"','"+usertype+"')";
            return db.ExNonQuery(cmd);
        }
        public DataTable getUsers()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from tbl_user";
             return db.ExeReader(cmd);

        }
        public DataTable getTester()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from tbl_user where usertype='Tester'";
            return db.ExeReader(cmd);


        }
        public int  AssignProject(string projectid,string userid,string assigneddate)
        {
             OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into tbl_project_assigned (project_id,user_id,assigned_date) values ('" + projectid + "','" + userid + "',TO_DATE('" + assigneddate+"','YYYY-MM-DD'))";
            return db.ExNonQuery(cmd);
        }

        public DataTable getAssignedTesters()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select distinct TBL_PROJECT_ASSIGNED.PROJECT_ID,TBL_PROJECT.PROJECT_NAME,tbl_project_assigned.user_id,TBL_USER.username,tbl_project_assigned.assigned_date from tbl_project,tbl_user,tbl_project_assigned where TBL_PROJECT.PROJECTID = TBL_PROJECT_ASSIGNED.PROJECT_ID and TBL_USER.USERID = tbl_project_assigned.user_id";
            return db.ExeReader(cmd);
        }
        ////to get the project assigned to specific testers.
       public DataTable getProjectByID(int userid)
        {

            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select project_name,project_id,userid from tbl_project,tbl_project_assigned,tbl_user where tbl_project.PROJECTID = tbl_project_assigned.PROJECT_ID and tbl_project_assigned.USER_ID = tbl_user.USERID and TBL_PROJECT_ASSIGNED.USER_ID = "+userid+"";
            return db.ExeReader(cmd);

        }

       public int AddBug(int userid, string registerdby, string bugtitle, string bugstatus,string bugdesc,string regdate,string projectname)
       {
           //adding bug information
           OracleCommand cmd = new OracleCommand();
           cmd.CommandType = CommandType.Text;
           cmd.CommandText = "insert into tbl_bugregister (bugid,userid,registeredby,bugtitle,bugstatus,bugdesc,regdate,project_name) values (bugid.nextval," + userid + ",'" + registerdby + "','"+bugtitle+"','" + bugstatus + "','" + bugdesc + "',TO_DATE('" + regdate + "','YYYY-MM-DD'),'"+projectname+"')";
           return db.ExNonQuery(cmd);
       }

       public DataTable showBugsAssignedByTester(int userid)
       {
           //get the bug list assigned by the current tester
           OracleCommand cmd = new OracleCommand();
           cmd.CommandType = CommandType.Text;
           cmd.CommandText = "select * from tbl_bugregister where userid ="+userid+"";
           return db.ExeReader(cmd);


       }
       public DataTable getDeveloper()
       {
           OracleCommand cmd = new OracleCommand();
           cmd.CommandType = CommandType.Text;
           cmd.CommandText = "select * from tbl_user where usertype='Developer'";
           return db.ExeReader(cmd);


       }

       public DataTable getDevelopername(int devloperid)
       {
                OracleCommand cmd = new OracleCommand();
           cmd.CommandType = CommandType.Text;
           cmd.CommandText = "select * from tbl_user where usertype='Developer' and userid="+devloperid+"";
           return db.ExeReader(cmd);
       }

       public int AssignBugtoDeveloper(int bugid, int userid, string assigneddate)
       {

           OracleCommand cmd = new OracleCommand();
           cmd.CommandType = CommandType.Text;
           cmd.CommandText = "insert into tbl_bugassigned (assignid,bugid,userid,assigneddate) values (assignid.nextval,'" + bugid + "','" + userid + "',TO_DATE('" + assigneddate + "','YYYY-MM-DD'))";
           return db.ExNonQuery(cmd);
       }



       public DataTable getBugsbyDeveloper(int userid)//creating a method to get the bugs assigned for specific developer
       {

           OracleCommand cmd = new OracleCommand();
           cmd.CommandType = CommandType.Text;
           cmd.CommandText = "select  tbl_bugregister.bugid,tbl_bugregister.bugtitle,tbl_bugregister.bugdesc,tbl_bugregister.project_name,tbl_bugassigned.assigneddate,tbl_bugassigned.enddate,tbl_bugregister.BUGSTATUS from tbl_bugregister,tbl_bugassigned where tbl_bugregister.BUGID = tbl_bugassigned.BUGID and tbl_bugassigned.USERID=" + userid + "";
           return db.ExeReader(cmd);
       

       }

       public int UpdateBugStatus(string status,int bugid)//method created to update the bug status
       {
           OracleCommand cmd = new OracleCommand();
           cmd.CommandType = CommandType.Text;
           cmd.CommandText = "update tbl_bugregister set bugstatus = '"+status+"' where bugid ='"+bugid+"'";
           return db.ExNonQuery(cmd);
       }

       
        //update the previous bug status and fix the code
        public int fixBugs(string bugdesc, int bugid)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "update tbl_bugregister set bugstatus = 'Fixed', bugdesc='"+bugdesc+"' where bugid ='" + bugid + "'";
            return db.ExNonQuery(cmd);
        }

        //get all the bugs that has been fixed by the developer
        public DataTable viewfixedbugs()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select tbl_bugregister.bugid,tbl_bugregister.bugtitle,tbl_bugregister.project_name,tbl_bugregister.regdate,tbl_bugassigned.assigneddate,tbl_bugassigned.enddate,tbl_bugregister.BUGSTATUS,tbl_user.username from tbl_bugregister,tbl_bugassigned,tbl_user where tbl_bugregister.BUGID = tbl_bugassigned.BUGID and tbl_bugassigned.userid= tbl_user.userid and tbl_bugregister.bugstatus = 'Fixed'";
            return db.ExeReader(cmd);
        }

        //get all the bugs that has been fixed by the developer
        public DataTable viewUnfixedBugs()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select tbl_bugregister.bugid,tbl_bugregister.bugtitle,tbl_bugregister.project_name,tbl_bugregister.regdate,tbl_bugassigned.assigneddate,tbl_bugassigned.enddate,tbl_bugregister.BUGSTATUS,tbl_user.username from tbl_bugregister,tbl_bugassigned,tbl_user where( tbl_bugregister.BUGID = tbl_bugassigned.BUGID and tbl_bugassigned.userid= tbl_user.userid and tbl_bugregister.bugstatus = 'Assigned')";
            return db.ExeReader(cmd);
        }
        public DataTable viewUnAssignedBugs()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select tbl_bugregister.bugid,tbl_bugregister.bugtitle,tbl_bugregister.project_name,tbl_bugregister.regdate,tbl_bugassigned.assigneddate,tbl_bugassigned.enddate,tbl_bugregister.BUGSTATUS,tbl_user.username from tbl_bugregister,tbl_bugassigned,tbl_user where( tbl_bugregister.BUGID = tbl_bugassigned.BUGID and tbl_bugassigned.userid= tbl_user.userid and tbl_bugregister.bugstatus = 'Open')";
            return db.ExeReader(cmd);
        }

        public int DeleteUser(int userid)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "delete from tbl_user where userid ='" + userid + "'";
            return db.ExNonQuery(cmd);
        }
        public int DeleteProject(int projectid)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "delete from tbl_project where projectid ='" + projectid + "'";
            return db.ExNonQuery(cmd);
        }

        public int addBugFixedDate(int bugid,string enddate)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "update tbl_bugassigned set enddate = TO_DATE('" + enddate + "','YYYY-MM-DD') where bugid ="+bugid+"";
            return db.ExNonQuery(cmd);
        }

     
       
        

    }
}
