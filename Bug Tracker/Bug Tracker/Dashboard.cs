﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace Bug_Tracker
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
           
        }
        private Point mousePoint;

        private void Dashboard_Load(object sender, EventArgs e)
        {
            userid.Text = "User ID: " + holdvalues.UserID.ToString();
        }

 

        private void btnaddprojects_Click(object sender, EventArgs e)
        {
            add_Projects1.BringToFront();
            panelHighlight.Height = btnaddprojects.Height;
            panelHighlight.Top = btnaddprojects.Top;

        }

        private void btnaddusers_Click(object sender, EventArgs e)
        {
            addUsers1.BringToFront();
            panelHighlight.Height = btnaddusers.Height;
            panelHighlight.Top = btnaddusers.Top;
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bunifuImageButton2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Dashboard_MouseMove(object sender, MouseEventArgs e)
        {
            //move the windows without border 
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point currentPos = Location;
                currentPos.Offset(e.X + mousePoint.X, e.Y + mousePoint.Y);
                this.Location = currentPos;
            }
        }

        private void Dashboard_MouseDown(object sender, MouseEventArgs e)
        {
            mousePoint = new Point(-e.X, -e.Y);

        }

        private void btnassign_Click(object sender, EventArgs e)
        {
            assignProject1.BringToFront();
            panelHighlight.Height = btnassign.Height;
            panelHighlight.Top = btnassign.Top;
        }

        private void assignProject1_Load(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void userid_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            bugreport1.BringToFront();
            panelHighlight.Height = btnReport.Height;
            panelHighlight.Top = btnReport.Top;

        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Close();
            Login l = new Login();
            l.Show();
        }

       
    }
}
