﻿namespace Bug_Tracker
{
    partial class AssignProject
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AssignProject));
            this.cmbprojects = new Bunifu.Framework.UI.BunifuDropdown();
            this.Project = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbtester = new Bunifu.Framework.UI.BunifuDropdown();
            this.btnassign = new Bunifu.Framework.UI.BunifuThinButton2();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbprojects
            // 
            this.cmbprojects.BackColor = System.Drawing.Color.Transparent;
            this.cmbprojects.BorderRadius = 3;
            this.cmbprojects.DisabledColor = System.Drawing.Color.Gray;
            this.cmbprojects.ForeColor = System.Drawing.Color.White;
            this.cmbprojects.Items = new string[0];
            this.cmbprojects.Location = new System.Drawing.Point(47, 71);
            this.cmbprojects.Name = "cmbprojects";
            this.cmbprojects.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
            this.cmbprojects.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.cmbprojects.selectedIndex = -1;
            this.cmbprojects.Size = new System.Drawing.Size(241, 35);
            this.cmbprojects.TabIndex = 5;
            // 
            // Project
            // 
            this.Project.AutoSize = true;
            this.Project.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.Project.Location = new System.Drawing.Point(42, 32);
            this.Project.Name = "Project";
            this.Project.Size = new System.Drawing.Size(71, 25);
            this.Project.TabIndex = 4;
            this.Project.Text = "Project";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label1.Location = new System.Drawing.Point(42, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tester";
            // 
            // cmbtester
            // 
            this.cmbtester.BackColor = System.Drawing.Color.Transparent;
            this.cmbtester.BorderRadius = 3;
            this.cmbtester.DisabledColor = System.Drawing.Color.Gray;
            this.cmbtester.ForeColor = System.Drawing.Color.White;
            this.cmbtester.Items = new string[0];
            this.cmbtester.Location = new System.Drawing.Point(47, 166);
            this.cmbtester.Name = "cmbtester";
            this.cmbtester.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
            this.cmbtester.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.cmbtester.selectedIndex = -1;
            this.cmbtester.Size = new System.Drawing.Size(241, 35);
            this.cmbtester.TabIndex = 5;
            // 
            // btnassign
            // 
            this.btnassign.ActiveBorderThickness = 1;
            this.btnassign.ActiveCornerRadius = 20;
            this.btnassign.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.btnassign.ActiveForecolor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnassign.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.btnassign.BackColor = System.Drawing.SystemColors.Control;
            this.btnassign.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnassign.BackgroundImage")));
            this.btnassign.ButtonText = "ASSIGN";
            this.btnassign.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnassign.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnassign.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnassign.IdleBorderThickness = 1;
            this.btnassign.IdleCornerRadius = 20;
            this.btnassign.IdleFillColor = System.Drawing.Color.SeaGreen;
            this.btnassign.IdleForecolor = System.Drawing.Color.White;
            this.btnassign.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.btnassign.Location = new System.Drawing.Point(47, 251);
            this.btnassign.Margin = new System.Windows.Forms.Padding(5);
            this.btnassign.Name = "btnassign";
            this.btnassign.Size = new System.Drawing.Size(135, 41);
            this.btnassign.TabIndex = 6;
            this.btnassign.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnassign.Click += new System.EventHandler(this.btnassign_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(318, 51);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(555, 241);
            this.dataGridView1.TabIndex = 7;
            // 
            // AssignProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnassign);
            this.Controls.Add(this.cmbtester);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbprojects);
            this.Controls.Add(this.Project);
            this.Name = "AssignProject";
            this.Size = new System.Drawing.Size(900, 509);
            this.Load += new System.EventHandler(this.AssignProject_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuDropdown cmbprojects;
        private System.Windows.Forms.Label Project;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuDropdown cmbtester;
        private Bunifu.Framework.UI.BunifuThinButton2 btnassign;
        private System.Windows.Forms.DataGridView dataGridView1;

    }
}
