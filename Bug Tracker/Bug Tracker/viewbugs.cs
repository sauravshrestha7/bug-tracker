﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ICSharpCode.TextEditor.Document;
using System.IO;
using BLL;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
namespace Bug_Tracker
{
    public partial class viewbugs : UserControl
    {
        public viewbugs()
        {
            InitializeComponent();
        }
        operations op = new operations();
      
       
        private void txtcode_Load(object sender, EventArgs e)
        {
            string dirc = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(dirc))
            {

                fsmp = new FileSyntaxModeProvider(dirc);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txtcode.SetHighlighting("C#");

            }

        
           
        }

        private void viewbugs_Load(object sender, EventArgs e)
        {
            LoadBugsforDeveloper();
        }

        public void LoadBugsforDeveloper()
        {

            DataTable dt = op.getBugsbyDeveloper(holdvalues.UserID);
            dataGridView1.DataSource = dt;
        }
        string bugid;
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bugid = dataGridView1.Rows[e.RowIndex].Cells["bugid"].Value.ToString();

            txtcode.Text = dataGridView1.Rows[e.RowIndex].Cells["bugdesc"].Value.ToString();
         
         
         
        }

     

        private void updatestatus_Click(object sender, EventArgs e)
        {
            if (bugid == "")
            {
                MessageBox.Show("Please select a bug");
            }
            else
            {
                op.UpdateBugStatus("Fixed", Convert.ToInt32(bugid));
                LoadBugsforDeveloper();
                MessageBox.Show("Status Updated");

            }
        }

       

        private void btnfix_Click(object sender, EventArgs e)
        {
            if (bugid == "")
            {
                MessageBox.Show("Please select a bug");
            }
             else
            {
            op.fixBugs(txtcode.Text, Convert.ToInt32(bugid));
            op.addBugFixedDate(Convert.ToInt32(bugid), DateTime.Now.ToString("yyyy-MM-dd"));
            LoadBugsforDeveloper();
            MessageBox.Show("Status Updated");
            }
        }
    }
}
