﻿namespace Bug_Tracker
{
    partial class AssignBug
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AssignBug));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnassign = new Bunifu.Framework.UI.BunifuThinButton2();
            this.cmbdevelopername = new Bunifu.Framework.UI.BunifuDropdown();
            this.label2 = new System.Windows.Forms.Label();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.txtdevelopername = new Bunifu.Framework.UI.BunifuMetroTextbox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(36, 67);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(769, 402);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Assign Bug ";
            // 
            // btnassign
            // 
            this.btnassign.ActiveBorderThickness = 1;
            this.btnassign.ActiveCornerRadius = 20;
            this.btnassign.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.btnassign.ActiveForecolor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnassign.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.btnassign.BackColor = System.Drawing.SystemColors.Control;
            this.btnassign.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnassign.BackgroundImage")));
            this.btnassign.ButtonText = "ASSIGN";
            this.btnassign.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnassign.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnassign.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnassign.IdleBorderThickness = 1;
            this.btnassign.IdleCornerRadius = 20;
            this.btnassign.IdleFillColor = System.Drawing.Color.SeaGreen;
            this.btnassign.IdleForecolor = System.Drawing.Color.White;
            this.btnassign.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.btnassign.Location = new System.Drawing.Point(836, 218);
            this.btnassign.Margin = new System.Windows.Forms.Padding(5);
            this.btnassign.Name = "btnassign";
            this.btnassign.Size = new System.Drawing.Size(135, 41);
            this.btnassign.TabIndex = 9;
            this.btnassign.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnassign.Click += new System.EventHandler(this.btnassign_Click);
            // 
            // cmbdevelopername
            // 
            this.cmbdevelopername.BackColor = System.Drawing.Color.Transparent;
            this.cmbdevelopername.BorderRadius = 3;
            this.cmbdevelopername.DisabledColor = System.Drawing.Color.Gray;
            this.cmbdevelopername.ForeColor = System.Drawing.Color.White;
            this.cmbdevelopername.Items = new string[0];
            this.cmbdevelopername.Location = new System.Drawing.Point(834, 77);
            this.cmbdevelopername.Name = "cmbdevelopername";
            this.cmbdevelopername.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
            this.cmbdevelopername.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.cmbdevelopername.selectedIndex = -1;
            this.cmbdevelopername.Size = new System.Drawing.Size(241, 35);
            this.cmbdevelopername.TabIndex = 8;
            this.cmbdevelopername.onItemSelected += new System.EventHandler(this.cmbdevelopername_onItemSelected);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label2.Location = new System.Drawing.Point(829, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Developer";
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(773, 33);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(32, 32);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bunifuImageButton1.TabIndex = 10;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // txtdevelopername
            // 
            this.txtdevelopername.BorderColorFocused = System.Drawing.Color.Green;
            this.txtdevelopername.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtdevelopername.BorderColorMouseHover = System.Drawing.Color.Gray;
            this.txtdevelopername.BorderThickness = 2;
            this.txtdevelopername.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtdevelopername.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtdevelopername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtdevelopername.isPassword = false;
            this.txtdevelopername.Location = new System.Drawing.Point(836, 142);
            this.txtdevelopername.Margin = new System.Windows.Forms.Padding(4);
            this.txtdevelopername.Name = "txtdevelopername";
            this.txtdevelopername.Size = new System.Drawing.Size(239, 41);
            this.txtdevelopername.TabIndex = 11;
            this.txtdevelopername.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // AssignBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtdevelopername);
            this.Controls.Add(this.bunifuImageButton1);
            this.Controls.Add(this.btnassign);
            this.Controls.Add(this.cmbdevelopername);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "AssignBug";
            this.Size = new System.Drawing.Size(1157, 564);
            this.Load += new System.EventHandler(this.AssignBug_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuThinButton2 btnassign;
        private Bunifu.Framework.UI.BunifuDropdown cmbdevelopername;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtdevelopername;

    }
}
