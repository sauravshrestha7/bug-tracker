﻿namespace Bug_Tracker
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.userid = new System.Windows.Forms.LinkLabel();
            this.panelHighlight = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnReport = new System.Windows.Forms.Button();
            this.btnassign = new System.Windows.Forms.Button();
            this.btnaddprojects = new System.Windows.Forms.Button();
            this.btnaddusers = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnclose = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnmin = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.bugreport1 = new Bug_Tracker.bugreport();
            this.assignProject1 = new Bug_Tracker.AssignProject();
            this.addUsers1 = new Bug_Tracker.AddUsers();
            this.add_Projects1 = new Bug_Tracker.Add_Projects();
            this.bunifuGradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnclose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.panel2);
            this.bunifuGradientPanel1.Controls.Add(this.userid);
            this.bunifuGradientPanel1.Controls.Add(this.panelHighlight);
            this.bunifuGradientPanel1.Controls.Add(this.btnLogout);
            this.bunifuGradientPanel1.Controls.Add(this.btnReport);
            this.bunifuGradientPanel1.Controls.Add(this.btnassign);
            this.bunifuGradientPanel1.Controls.Add(this.btnaddprojects);
            this.bunifuGradientPanel1.Controls.Add(this.btnaddusers);
            this.bunifuGradientPanel1.Controls.Add(this.panel1);
            this.bunifuGradientPanel1.Controls.Add(this.lblName);
            this.bunifuGradientPanel1.Controls.Add(this.pictureBox1);
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.CornflowerBlue;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 35);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(248, 751);
            this.bunifuGradientPanel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(249, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1191, 730);
            this.panel2.TabIndex = 3;
            // 
            // userid
            // 
            this.userid.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.userid.AutoSize = true;
            this.userid.BackColor = System.Drawing.Color.Transparent;
            this.userid.LinkColor = System.Drawing.Color.White;
            this.userid.Location = new System.Drawing.Point(78, 122);
            this.userid.Name = "userid";
            this.userid.Size = new System.Drawing.Size(57, 13);
            this.userid.TabIndex = 5;
            this.userid.TabStop = true;
            this.userid.Text = "Edit Profile";
            this.userid.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.userid_LinkClicked);
            // 
            // panelHighlight
            // 
            this.panelHighlight.BackColor = System.Drawing.Color.ForestGreen;
            this.panelHighlight.Location = new System.Drawing.Point(0, 181);
            this.panelHighlight.Name = "panelHighlight";
            this.panelHighlight.Size = new System.Drawing.Size(10, 53);
            this.panelHighlight.TabIndex = 5;
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLogout.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.Image")));
            this.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Location = new System.Drawing.Point(25, 406);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(190, 49);
            this.btnLogout.TabIndex = 4;
            this.btnLogout.Text = "LOGOUT";
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.Transparent;
            this.btnReport.FlatAppearance.BorderSize = 0;
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReport.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReport.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnReport.Image = ((System.Drawing.Image)(resources.GetObject("btnReport.Image")));
            this.btnReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReport.Location = new System.Drawing.Point(25, 348);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(186, 52);
            this.btnReport.TabIndex = 4;
            this.btnReport.Text = "REPORT";
            this.btnReport.UseVisualStyleBackColor = false;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnassign
            // 
            this.btnassign.BackColor = System.Drawing.Color.Transparent;
            this.btnassign.FlatAppearance.BorderSize = 0;
            this.btnassign.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnassign.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnassign.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnassign.Image = ((System.Drawing.Image)(resources.GetObject("btnassign.Image")));
            this.btnassign.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnassign.Location = new System.Drawing.Point(25, 294);
            this.btnassign.Name = "btnassign";
            this.btnassign.Size = new System.Drawing.Size(196, 48);
            this.btnassign.TabIndex = 4;
            this.btnassign.Text = "ASSIGN PROJECT";
            this.btnassign.UseVisualStyleBackColor = false;
            this.btnassign.Click += new System.EventHandler(this.btnassign_Click);
            // 
            // btnaddprojects
            // 
            this.btnaddprojects.BackColor = System.Drawing.Color.Transparent;
            this.btnaddprojects.FlatAppearance.BorderSize = 0;
            this.btnaddprojects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnaddprojects.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddprojects.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddprojects.Image = ((System.Drawing.Image)(resources.GetObject("btnaddprojects.Image")));
            this.btnaddprojects.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddprojects.Location = new System.Drawing.Point(25, 235);
            this.btnaddprojects.Name = "btnaddprojects";
            this.btnaddprojects.Size = new System.Drawing.Size(186, 53);
            this.btnaddprojects.TabIndex = 4;
            this.btnaddprojects.Text = "ADD PROJECTS";
            this.btnaddprojects.UseVisualStyleBackColor = false;
            this.btnaddprojects.Click += new System.EventHandler(this.btnaddprojects_Click);
            // 
            // btnaddusers
            // 
            this.btnaddusers.BackColor = System.Drawing.Color.Transparent;
            this.btnaddusers.FlatAppearance.BorderSize = 0;
            this.btnaddusers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnaddusers.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddusers.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddusers.Image = ((System.Drawing.Image)(resources.GetObject("btnaddusers.Image")));
            this.btnaddusers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddusers.Location = new System.Drawing.Point(25, 181);
            this.btnaddusers.Name = "btnaddusers";
            this.btnaddusers.Size = new System.Drawing.Size(186, 53);
            this.btnaddusers.TabIndex = 4;
            this.btnaddusers.Text = "ADD USERS";
            this.btnaddusers.UseVisualStyleBackColor = false;
            this.btnaddusers.Click += new System.EventHandler(this.btnaddusers_Click);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(249, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(736, 19);
            this.panel1.TabIndex = 3;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblName.Location = new System.Drawing.Point(64, 109);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(86, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Saurav Shrestha";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(62, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(88, 86);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnclose
            // 
            this.btnclose.BackColor = System.Drawing.Color.Transparent;
            this.btnclose.Image = ((System.Drawing.Image)(resources.GetObject("btnclose.Image")));
            this.btnclose.ImageActive = null;
            this.btnclose.Location = new System.Drawing.Point(1868, 0);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(50, 48);
            this.btnclose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnclose.TabIndex = 2;
            this.btnclose.TabStop = false;
            this.btnclose.Zoom = 10;
            // 
            // btnmin
            // 
            this.btnmin.BackColor = System.Drawing.Color.Transparent;
            this.btnmin.Image = ((System.Drawing.Image)(resources.GetObject("btnmin.Image")));
            this.btnmin.ImageActive = null;
            this.btnmin.Location = new System.Drawing.Point(1812, 0);
            this.btnmin.Name = "btnmin";
            this.btnmin.Size = new System.Drawing.Size(50, 48);
            this.btnmin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnmin.TabIndex = 2;
            this.btnmin.TabStop = false;
            this.btnmin.Zoom = 10;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this;
            this.bunifuDragControl1.Vertical = true;
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(1458, 6);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(32, 32);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bunifuImageButton1.TabIndex = 3;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.Location = new System.Drawing.Point(1424, 6);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(32, 32);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bunifuImageButton2.TabIndex = 3;
            this.bunifuImageButton2.TabStop = false;
            this.bunifuImageButton2.Zoom = 10;
            this.bunifuImageButton2.Click += new System.EventHandler(this.bunifuImageButton2_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.bunifuImageButton2);
            this.panel3.Controls.Add(this.bunifuImageButton1);
            this.panel3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel3.Location = new System.Drawing.Point(-1, -4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1497, 40);
            this.panel3.TabIndex = 7;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(712, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "BUG TRACKER";
            // 
            // bugreport1
            // 
            this.bugreport1.Location = new System.Drawing.Point(296, 74);
            this.bugreport1.Name = "bugreport1";
            this.bugreport1.Size = new System.Drawing.Size(1153, 632);
            this.bugreport1.TabIndex = 8;
            // 
            // assignProject1
            // 
            this.assignProject1.Location = new System.Drawing.Point(296, 86);
            this.assignProject1.Name = "assignProject1";
            this.assignProject1.Size = new System.Drawing.Size(1186, 651);
            this.assignProject1.TabIndex = 6;
            this.assignProject1.Load += new System.EventHandler(this.assignProject1_Load);
            // 
            // addUsers1
            // 
            this.addUsers1.Location = new System.Drawing.Point(284, 95);
            this.addUsers1.Name = "addUsers1";
            this.addUsers1.Size = new System.Drawing.Size(1186, 592);
            this.addUsers1.TabIndex = 5;
            // 
            // add_Projects1
            // 
            this.add_Projects1.Location = new System.Drawing.Point(284, 86);
            this.add_Projects1.Name = "add_Projects1";
            this.add_Projects1.Size = new System.Drawing.Size(1186, 560);
            this.add_Projects1.TabIndex = 4;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1494, 786);
            this.Controls.Add(this.bugreport1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.assignProject1);
            this.Controls.Add(this.addUsers1);
            this.Controls.Add(this.add_Projects1);
            this.Controls.Add(this.btnmin);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Dashboard_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Dashboard_MouseMove);
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.bunifuGradientPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnclose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private System.Windows.Forms.LinkLabel userid;
        private System.Windows.Forms.Panel panelHighlight;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Button btnassign;
        private System.Windows.Forms.Button btnaddusers;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.Framework.UI.BunifuImageButton btnmin;
        private Bunifu.Framework.UI.BunifuImageButton btnclose;
        private System.Windows.Forms.Button btnaddprojects;
        public System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Add_Projects add_Projects1;
        private AddUsers addUsers1;
        private AssignProject assignProject1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private bugreport bugreport1;
    }
}