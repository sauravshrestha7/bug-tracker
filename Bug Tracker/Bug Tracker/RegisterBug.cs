﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ICSharpCode.TextEditor.Document;
using System.IO;
using BLL;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
namespace Bug_Tracker
{
    public partial class RegisterBug : UserControl
    {
        public RegisterBug()
        {
            InitializeComponent();
        }

        private void bunifuMetroTextbox5_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void btnAttach_Click(object sender, EventArgs e)
        {
           
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            operations op = new operations();
            string date = DateTime.Now.ToString("yyyy-MM-dd");

            if (cmbproject.selectedIndex < 0)
            {
                MessageBox.Show("Please Select Project Name");
            }
            else
            {
                int isbugAdded = op.AddBug(Convert.ToInt32(txtuserid.Text), txtregisteredby.Text, txtbugtitile.Text, cmbBugStatus.selectedValue, txtcode.Text, date, cmbproject.selectedValue);
                if (isbugAdded > 0)
                {
                    MessageBox.Show("Bug Added Succesfully");
                    getProjectbyUser();
                }
            }
        }

        private void RegisterBug_Load(object sender, EventArgs e)
        {
            Tester t = new Tester();//creating an instance of the clase
            txtregisteredby.Text = holdvalues.UserName;
            txtuserid.Text = holdvalues.UserID.ToString();
            txtuserid.Enabled = false;
            txtregisteredby.Enabled = false;

            operations op = new operations();
            DataTable dt =op.getProjectByID(holdvalues.UserID);

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                cmbproject.AddItem(Convert.ToString(dt.Rows[i]["project_name"]));
            }
            getProjectbyUser();

        }

        private void txtcode_Load(object sender, EventArgs e)
        {
             
            string dirc = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(dirc))
            {

                fsmp = new FileSyntaxModeProvider(dirc);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txtcode.SetHighlighting("C#");

            }
        }
        
        public void getProjectbyUser()
        {

            //get the list of bug added by the current tester
            operations op = new operations();
            DataTable dt = op.showBugsAssignedByTester(holdvalues.UserID);
            dataGridView1.DataSource = dt;

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            string bugid = dataGridView1.Rows[e.RowIndex].Cells["bugid"].Value.ToString();

            txtcode.Text = dataGridView1.Rows[e.RowIndex].Cells["bugdesc"].Value.ToString();

        }
    }
}
