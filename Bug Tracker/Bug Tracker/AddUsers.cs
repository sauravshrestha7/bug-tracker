﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using Oracle.ManagedDataAccess.Client;
namespace Bug_Tracker
{
    public partial class AddUsers : UserControl
    {
        public AddUsers()
        {
            InitializeComponent();
        }
        operations op = new operations();
        private void AddUsers_Load(object sender, EventArgs e)
        {
            //get all the user information 
            LoadData();
            cmbUserType.selectedIndex = 0;
     
        }
       
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtfname.Text.Equals("") || txtlastname.Text.Equals("") || txtaddress.Text.Equals("") || txtpassword.Text.Equals("") || txtphone.Text.Equals("") || txtusername.Text.Equals("") ||cmbUserType.selectedIndex<0)
            {
                MessageBox.Show("Fill All the fields");

            }

            else
            {
                int isAdduser = op.AddUsers(txtfname.Text, txtlastname.Text, txtaddress.Text, txtphone.Text, txtusername.Text, txtpassword.Text, cmbUserType.selectedValue);
                if (isAdduser > 0)
                {
                    MessageBox.Show("Data Added Succesfully");
                    LoadData();
                }
            }

        }
        //creating method to populate data grid view
        public void LoadData()
        {
            DataTable dt = op.getUsers();
            dataGridView1.DataSource = dt;
        }

        private void deleteUser_Click(object sender, EventArgs e)
        {
            if (userid == null)
            {
                MessageBox.Show("Please Select the user");
            }
            else
            {
                int a = op.DeleteUser(Convert.ToInt32(userid));
                if (a > 0)
                {
                    MessageBox.Show("Data deleted succesfully");
                    LoadData();
                    userid = null;
                }
            }
        }
        string userid;
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            userid = dataGridView1.Rows[e.RowIndex].Cells["userid"].Value.ToString();
        }

    }
}
