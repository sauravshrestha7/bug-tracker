﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using Oracle.ManagedDataAccess.Client;

namespace Bug_Tracker
{
    public partial class AssignProject : UserControl
    {
        public AssignProject()
        {
            InitializeComponent();
        }
        operations op = new operations();
        string userid;
        private void AssignProject_Load(object sender, EventArgs e)
        {
            DataTable dtproject = op.getAllProjects();
            //population the dropdown box with different types of categories
            for (int i = 0; i < dtproject.Rows.Count; i++)
            {

                cmbprojects.AddItem(Convert.ToString(dtproject.Rows[i]["project_name"]));

            }
            cmbprojects.selectedIndex = 0;
           

            DataTable dtester = op.getTester();
            for (int i = 0; i < dtester.Rows.Count; i++)
            {

                cmbtester.AddItem(Convert.ToString(dtester.Rows[i]["first_name"])+" "+Convert.ToString(dtester.Rows[i]["last_name"]));
                
            }

            cmbtester.selectedIndex = 0;
             userid = dtester.Rows[cmbtester.selectedIndex]["userid"].ToString();
             LoadData();

        }

        private void btnassign_Click(object sender, EventArgs e)
        {
            //getting the userid and project id of the select item in the combo box 
            DataTable dt = op.getTester();
            string userid = dt.Rows[cmbtester.selectedIndex]["userid"].ToString();
            DataTable dt1 = op.getAllProjects();
            string projectid = dt1.Rows[cmbprojects.selectedIndex]["projectid"].ToString();

            string date = DateTime.Now.ToString("yyyy-MM-dd");

          

            int assignProject = op.AssignProject(projectid, userid, date);
            if (assignProject > 0)
            {
                MessageBox.Show("Successfully Added");
            }


        }
        //displaying the project assigned data 
        public void LoadData()
        {
            DataTable dt = op.getAssignedTesters();
            dataGridView1.DataSource = dt;

        }
    }
}
