﻿namespace Bug_Tracker
{
    partial class Add_Projects
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Projects));
            this.txtversion = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtprojectname = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtprojectdesc = new System.Windows.Forms.RichTextBox();
            this.btnAdd = new Bunifu.Framework.UI.BunifuThinButton2();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.deleteUser = new Bunifu.Framework.UI.BunifuThinButton2();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtversion
            // 
            this.txtversion.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtversion.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtversion.BorderColorMouseHover = System.Drawing.Color.SlateGray;
            this.txtversion.BorderThickness = 3;
            this.txtversion.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtversion.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtversion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtversion.isPassword = false;
            this.txtversion.Location = new System.Drawing.Point(44, 376);
            this.txtversion.Margin = new System.Windows.Forms.Padding(4);
            this.txtversion.Name = "txtversion";
            this.txtversion.Size = new System.Drawing.Size(241, 44);
            this.txtversion.TabIndex = 9;
            this.txtversion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label3.Location = new System.Drawing.Point(39, 347);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Version";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label2.Location = new System.Drawing.Point(39, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Description";
            // 
            // txtprojectname
            // 
            this.txtprojectname.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtprojectname.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtprojectname.BorderColorMouseHover = System.Drawing.Color.Gray;
            this.txtprojectname.BorderThickness = 3;
            this.txtprojectname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtprojectname.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtprojectname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtprojectname.isPassword = false;
            this.txtprojectname.Location = new System.Drawing.Point(44, 80);
            this.txtprojectname.Margin = new System.Windows.Forms.Padding(4);
            this.txtprojectname.Name = "txtprojectname";
            this.txtprojectname.Size = new System.Drawing.Size(241, 44);
            this.txtprojectname.TabIndex = 7;
            this.txtprojectname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label1.Location = new System.Drawing.Point(39, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Project Name";
            // 
            // txtprojectdesc
            // 
            this.txtprojectdesc.Location = new System.Drawing.Point(44, 176);
            this.txtprojectdesc.Name = "txtprojectdesc";
            this.txtprojectdesc.Size = new System.Drawing.Size(250, 146);
            this.txtprojectdesc.TabIndex = 10;
            this.txtprojectdesc.Text = "";
            // 
            // btnAdd
            // 
            this.btnAdd.ActiveBorderThickness = 1;
            this.btnAdd.ActiveCornerRadius = 20;
            this.btnAdd.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.btnAdd.ActiveForecolor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAdd.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.btnAdd.BackColor = System.Drawing.SystemColors.Control;
            this.btnAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAdd.BackgroundImage")));
            this.btnAdd.ButtonText = "ADD";
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnAdd.IdleBorderThickness = 1;
            this.btnAdd.IdleCornerRadius = 20;
            this.btnAdd.IdleFillColor = System.Drawing.Color.SeaGreen;
            this.btnAdd.IdleForecolor = System.Drawing.Color.White;
            this.btnAdd.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.btnAdd.Location = new System.Drawing.Point(44, 469);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(5);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(114, 41);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(359, 51);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(550, 292);
            this.dataGridView1.TabIndex = 13;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this;
            this.bunifuDragControl1.Vertical = true;
            // 
            // deleteUser
            // 
            this.deleteUser.ActiveBorderThickness = 1;
            this.deleteUser.ActiveCornerRadius = 20;
            this.deleteUser.ActiveFillColor = System.Drawing.Color.Red;
            this.deleteUser.ActiveForecolor = System.Drawing.SystemColors.ButtonHighlight;
            this.deleteUser.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.deleteUser.BackColor = System.Drawing.SystemColors.Control;
            this.deleteUser.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("deleteUser.BackgroundImage")));
            this.deleteUser.ButtonText = "DELETE";
            this.deleteUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteUser.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteUser.ForeColor = System.Drawing.Color.SeaGreen;
            this.deleteUser.IdleBorderThickness = 1;
            this.deleteUser.IdleCornerRadius = 20;
            this.deleteUser.IdleFillColor = System.Drawing.Color.Red;
            this.deleteUser.IdleForecolor = System.Drawing.Color.White;
            this.deleteUser.IdleLineColor = System.Drawing.Color.Red;
            this.deleteUser.Location = new System.Drawing.Point(180, 469);
            this.deleteUser.Margin = new System.Windows.Forms.Padding(5);
            this.deleteUser.Name = "deleteUser";
            this.deleteUser.Size = new System.Drawing.Size(114, 41);
            this.deleteUser.TabIndex = 14;
            this.deleteUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.deleteUser.Click += new System.EventHandler(this.deleteUser_Click);
            // 
            // Add_Projects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.deleteUser);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtprojectdesc);
            this.Controls.Add(this.txtversion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtprojectname);
            this.Controls.Add(this.label1);
            this.Name = "Add_Projects";
            this.Size = new System.Drawing.Size(944, 560);
            this.Load += new System.EventHandler(this.Add_Projects_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuMetroTextbox txtversion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtprojectname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtprojectdesc;
        private Bunifu.Framework.UI.BunifuThinButton2 btnAdd;
        private System.Windows.Forms.DataGridView dataGridView1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuThinButton2 deleteUser;
    }
}
