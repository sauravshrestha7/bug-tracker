﻿namespace Bug_Tracker
{
    partial class viewbugs
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(viewbugs));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txtcode = new ICSharpCode.TextEditor.TextEditorControl();
            this.label1 = new System.Windows.Forms.Label();
            this.btnfix = new Bunifu.Framework.UI.BunifuThinButton2();
            this.cmbBugStatus = new Bunifu.Framework.UI.BunifuDropdown();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(17, 52);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(534, 249);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // txtcode
            // 
            this.txtcode.IsReadOnly = false;
            this.txtcode.Location = new System.Drawing.Point(561, 69);
            this.txtcode.Name = "txtcode";
            this.txtcode.Size = new System.Drawing.Size(643, 463);
            this.txtcode.TabIndex = 8;
            this.txtcode.Text = "textEditorControl1";
            this.txtcode.Load += new System.EventHandler(this.txtcode_Load);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(557, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Code";
            // 
            // btnfix
            // 
            this.btnfix.ActiveBorderThickness = 1;
            this.btnfix.ActiveCornerRadius = 20;
            this.btnfix.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.btnfix.ActiveForecolor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnfix.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.btnfix.BackColor = System.Drawing.SystemColors.Control;
            this.btnfix.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnfix.BackgroundImage")));
            this.btnfix.ButtonText = "FIX BUG";
            this.btnfix.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnfix.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfix.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnfix.IdleBorderThickness = 1;
            this.btnfix.IdleCornerRadius = 20;
            this.btnfix.IdleFillColor = System.Drawing.Color.SeaGreen;
            this.btnfix.IdleForecolor = System.Drawing.Color.White;
            this.btnfix.IdleLineColor = System.Drawing.Color.SeaGreen;
            this.btnfix.Location = new System.Drawing.Point(406, 315);
            this.btnfix.Margin = new System.Windows.Forms.Padding(5);
            this.btnfix.Name = "btnfix";
            this.btnfix.Size = new System.Drawing.Size(135, 41);
            this.btnfix.TabIndex = 10;
            this.btnfix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnfix.Click += new System.EventHandler(this.btnfix_Click);
            // 
            // cmbBugStatus
            // 
            this.cmbBugStatus.BackColor = System.Drawing.Color.Transparent;
            this.cmbBugStatus.BorderRadius = 3;
            this.cmbBugStatus.DisabledColor = System.Drawing.Color.Gray;
            this.cmbBugStatus.ForeColor = System.Drawing.Color.White;
            this.cmbBugStatus.Items = new string[] {
        "Fixed"};
            this.cmbBugStatus.Location = new System.Drawing.Point(134, 315);
            this.cmbBugStatus.Name = "cmbBugStatus";
            this.cmbBugStatus.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
            this.cmbBugStatus.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.cmbBugStatus.selectedIndex = -1;
            this.cmbBugStatus.Size = new System.Drawing.Size(241, 35);
            this.cmbBugStatus.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 320);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 24);
            this.label6.TabIndex = 11;
            this.label6.Text = "Bug Status";
            // 
            // viewbugs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cmbBugStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnfix);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtcode);
            this.Controls.Add(this.dataGridView1);
            this.Name = "viewbugs";
            this.Size = new System.Drawing.Size(1223, 588);
            this.Load += new System.EventHandler(this.viewbugs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private ICSharpCode.TextEditor.TextEditorControl txtcode;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuThinButton2 btnfix;
        private Bunifu.Framework.UI.BunifuDropdown cmbBugStatus;
        private System.Windows.Forms.Label label6;
    }
}
