﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace Bug_Tracker
{
    public partial class AssignBug : UserControl
    {
        public AssignBug()
        {
            InitializeComponent();
        }
        operations op = new operations();
        private void AssignBug_Load(object sender, EventArgs e)
        {
            getBugbyUser();
            getDeveloper();




       
            


        }

        public void getBugbyUser()
        {

            //get the list of bug added by the current tester
            operations op = new operations();
            DataTable dt = op.showBugsAssignedByTester(holdvalues.UserID);
            dataGridView1.DataSource = dt;

        }

        public void getDeveloper()
        {
            DataTable dt = op.getDeveloper();
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                cmbdevelopername.AddItem(Convert.ToString(dt.Rows[i]["userid"]));

            }
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            getBugbyUser();
          
        }

        private void cmbdevelopername_onItemSelected(object sender, EventArgs e)
        {
            string id = cmbdevelopername.selectedValue;
            DataTable dt = op.getDevelopername(Convert.ToInt32(id));

            txtdevelopername.Text = dt.Rows[0]["username"].ToString();
            


        }
        string bugid;
        string bugstatus;
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
             bugid = dataGridView1.Rows[e.RowIndex].Cells["bugid"].Value.ToString();
             bugstatus = dataGridView1.Rows[e.RowIndex].Cells["bugstatus"].Value.ToString();
          
        }

        private void btnassign_Click(object sender, EventArgs e)
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            if (bugstatus == "Assigned")
            {
                MessageBox.Show("Bug Already Assigned");
            }
            else
            {
                int a = op.AssignBugtoDeveloper(Convert.ToInt32(bugid), Convert.ToInt32(cmbdevelopername.selectedValue), date);
                if (a > 0)
                {
                    op.UpdateBugStatus("Assigned", Convert.ToInt32(bugid));
                    getBugbyUser();
                    MessageBox.Show("Status Updated");
                    MessageBox.Show("Bug assigned succesfully");

                }

         

            }

        }
    }
}
