﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using Oracle.ManagedDataAccess.Client;

namespace Bug_Tracker
{
    public partial class Add_Projects : UserControl
    {
        public Add_Projects()
        {
            InitializeComponent();
        }
        operations op = new operations();
       

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtprojectdesc.Text.Equals("") || txtprojectname.Text.Equals("") || txtversion.Text.Equals(""))
            {
                MessageBox.Show("Enter all fields");
            }

            else
            {
                int isAdd = op.AddProjects(txtprojectname.Text, txtprojectdesc.Text, txtversion.Text);
                if (isAdd > 0)
                {
                    MessageBox.Show("Project Added Succesfully");
                    Loaddata();
                }
            }
        }

        private void Add_Projects_Load(object sender, EventArgs e)
        {
            Loaddata();

        }
        public void Loaddata()
        {
        DataTable dt = op.getAllProjects();
            dataGridView1.DataSource = dt;
        }
        string projectid;
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            projectid = dataGridView1.Rows[e.RowIndex].Cells["projectid"].Value.ToString();
        }

        private void deleteUser_Click(object sender, EventArgs e)
        {
            if (projectid == null)
            {
                MessageBox.Show("Please Select a project");
            }
            else
            {
                int a = op.DeleteProject(Convert.ToInt32(projectid));
                if (a > 0)
                {
                    MessageBox.Show("Data deleted succesfully");
                    Loaddata();
                    projectid = null;
                }
            }
        }
    }
}
