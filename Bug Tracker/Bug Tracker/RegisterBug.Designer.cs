﻿namespace Bug_Tracker
{
    partial class RegisterBug
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterBug));
            this.label1 = new System.Windows.Forms.Label();
            this.txtuserid = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.txtregisteredby = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.txtbugtitile = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAttach = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnAdd = new Bunifu.Framework.UI.BunifuImageButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtcode = new ICSharpCode.TextEditor.TextEditorControl();
            this.cmbBugStatus = new Bunifu.Framework.UI.BunifuDropdown();
            this.cmbproject = new Bunifu.Framework.UI.BunifuDropdown();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.btnAttach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(490, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bug Registration";
            // 
            // txtuserid
            // 
            this.txtuserid.BorderColorFocused = System.Drawing.Color.Green;
            this.txtuserid.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtuserid.BorderColorMouseHover = System.Drawing.Color.Gray;
            this.txtuserid.BorderThickness = 2;
            this.txtuserid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtuserid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtuserid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtuserid.isPassword = false;
            this.txtuserid.Location = new System.Drawing.Point(232, 136);
            this.txtuserid.Margin = new System.Windows.Forms.Padding(4);
            this.txtuserid.Name = "txtuserid";
            this.txtuserid.Size = new System.Drawing.Size(239, 41);
            this.txtuserid.TabIndex = 1;
            this.txtuserid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtregisteredby
            // 
            this.txtregisteredby.BorderColorFocused = System.Drawing.Color.Green;
            this.txtregisteredby.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtregisteredby.BorderColorMouseHover = System.Drawing.Color.Gray;
            this.txtregisteredby.BorderThickness = 2;
            this.txtregisteredby.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtregisteredby.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtregisteredby.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtregisteredby.isPassword = false;
            this.txtregisteredby.Location = new System.Drawing.Point(232, 202);
            this.txtregisteredby.Margin = new System.Windows.Forms.Padding(4);
            this.txtregisteredby.Name = "txtregisteredby";
            this.txtregisteredby.Size = new System.Drawing.Size(239, 41);
            this.txtregisteredby.TabIndex = 1;
            this.txtregisteredby.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtbugtitile
            // 
            this.txtbugtitile.BorderColorFocused = System.Drawing.Color.Green;
            this.txtbugtitile.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbugtitile.BorderColorMouseHover = System.Drawing.Color.Gray;
            this.txtbugtitile.BorderThickness = 2;
            this.txtbugtitile.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtbugtitile.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtbugtitile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtbugtitile.isPassword = false;
            this.txtbugtitile.Location = new System.Drawing.Point(232, 268);
            this.txtbugtitile.Margin = new System.Windows.Forms.Padding(4);
            this.txtbugtitile.Name = "txtbugtitile";
            this.txtbugtitile.Size = new System.Drawing.Size(239, 41);
            this.txtbugtitile.TabIndex = 1;
            this.txtbugtitile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(86, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 24);
            this.label2.TabIndex = 5;
            this.label2.Text = "Project Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(86, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "UserID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(86, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 24);
            this.label4.TabIndex = 5;
            this.label4.Text = "Registered By";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(86, 285);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 24);
            this.label5.TabIndex = 5;
            this.label5.Text = "Bug Title";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(86, 342);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 24);
            this.label6.TabIndex = 5;
            this.label6.Text = "Bug Status";
            // 
            // btnAttach
            // 
            this.btnAttach.BackColor = System.Drawing.Color.Transparent;
            this.btnAttach.Image = ((System.Drawing.Image)(resources.GetObject("btnAttach.Image")));
            this.btnAttach.ImageActive = null;
            this.btnAttach.Location = new System.Drawing.Point(969, 393);
            this.btnAttach.Name = "btnAttach";
            this.btnAttach.Size = new System.Drawing.Size(53, 39);
            this.btnAttach.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnAttach.TabIndex = 6;
            this.btnAttach.TabStop = false;
            this.btnAttach.Zoom = 10;
            this.btnAttach.Click += new System.EventHandler(this.btnAttach_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageActive = null;
            this.btnAdd.Location = new System.Drawing.Point(905, 393);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(53, 39);
            this.btnAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnAdd.TabIndex = 6;
            this.btnAdd.TabStop = false;
            this.btnAdd.Zoom = 10;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // txtcode
            // 
            this.txtcode.IsReadOnly = false;
            this.txtcode.Location = new System.Drawing.Point(529, 76);
            this.txtcode.Name = "txtcode";
            this.txtcode.Size = new System.Drawing.Size(502, 311);
            this.txtcode.TabIndex = 7;
            this.txtcode.Text = "textEditorControl1";
            this.txtcode.Load += new System.EventHandler(this.txtcode_Load);
            // 
            // cmbBugStatus
            // 
            this.cmbBugStatus.BackColor = System.Drawing.Color.Transparent;
            this.cmbBugStatus.BorderRadius = 3;
            this.cmbBugStatus.DisabledColor = System.Drawing.Color.Gray;
            this.cmbBugStatus.ForeColor = System.Drawing.Color.White;
            this.cmbBugStatus.Items = new string[] {
        "Open",
        "Assigned",
        "Fixed"};
            this.cmbBugStatus.Location = new System.Drawing.Point(232, 331);
            this.cmbBugStatus.Name = "cmbBugStatus";
            this.cmbBugStatus.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
            this.cmbBugStatus.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.cmbBugStatus.selectedIndex = -1;
            this.cmbBugStatus.Size = new System.Drawing.Size(241, 35);
            this.cmbBugStatus.TabIndex = 9;
            // 
            // cmbproject
            // 
            this.cmbproject.BackColor = System.Drawing.Color.Transparent;
            this.cmbproject.BorderRadius = 3;
            this.cmbproject.DisabledColor = System.Drawing.Color.Gray;
            this.cmbproject.ForeColor = System.Drawing.Color.White;
            this.cmbproject.Items = new string[0];
            this.cmbproject.Location = new System.Drawing.Point(230, 76);
            this.cmbproject.Name = "cmbproject";
            this.cmbproject.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(48)))), ((int)(((byte)(64)))));
            this.cmbproject.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.cmbproject.selectedIndex = -1;
            this.cmbproject.Size = new System.Drawing.Size(241, 35);
            this.cmbproject.TabIndex = 9;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(90, 455);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(932, 196);
            this.dataGridView1.TabIndex = 10;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // RegisterBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmbproject);
            this.Controls.Add(this.cmbBugStatus);
            this.Controls.Add(this.txtcode);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnAttach);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtbugtitile);
            this.Controls.Add(this.txtregisteredby);
            this.Controls.Add(this.txtuserid);
            this.Controls.Add(this.label1);
            this.Name = "RegisterBug";
            this.Size = new System.Drawing.Size(1107, 671);
            this.Load += new System.EventHandler(this.RegisterBug_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnAttach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtuserid;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtregisteredby;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtbugtitile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuImageButton btnAttach;
        private Bunifu.Framework.UI.BunifuImageButton btnAdd;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private ICSharpCode.TextEditor.TextEditorControl txtcode;
        private Bunifu.Framework.UI.BunifuDropdown cmbBugStatus;
        private Bunifu.Framework.UI.BunifuDropdown cmbproject;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}
