﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace Bug_Tracker
{
    public partial class bugreport : UserControl
    {
        public bugreport()
        {
            InitializeComponent();
        }
        operations op = new operations();

        private void bugreport_Load(object sender, EventArgs e)
        {
            showFixed();
            showAssigned();
            showUnassigned();

        }

        public void showFixed()
        {
            DataTable dt = op.viewfixedbugs();
            dataGridView1.DataSource = dt;
        }
        public void showAssigned()
        {
            DataTable dt1 = op.viewUnfixedBugs();
            dataGridView2.DataSource = dt1;
        }
        public void showUnassigned()
        {
            DataTable dt = op.viewUnAssignedBugs();
            dataGridView3.DataSource = dt;
        }

 

       


    }
}
