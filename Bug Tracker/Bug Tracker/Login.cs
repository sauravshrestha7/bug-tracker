﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using BLL;
namespace Bug_Tracker
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        holdvalues h = new holdvalues();

        public string username;
        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            //get data table from BLL
            operations op = new operations();
            if (cmbUsertype.selectedIndex<0)
            {
                MessageBox.Show("Please Select Login Type");
            }
            else
            {

                DataTable dt = op.login(txtuserid.Text, txtpassword.Text,cmbUsertype.selectedValue);
                if (dt.Rows.Count > 0)//open dashboard when dt has more than 0 rows
                {
                    
                    MessageBox.Show("Login success");

                    if (dt.Rows[0]["usertype"].ToString() == "Project Manager")
                    {
                        Dashboard pg = new Dashboard();
                        pg.lblName.Text = "Username: " + txtuserid.Text;
                        holdvalues.UserID = Convert.ToInt32(dt.Rows[0]["userid"]);
                        holdvalues.UserName = dt.Rows[0]["username"].ToString();
                        pg.Show();
                        this.Hide();

                    }
                    else if (dt.Rows[0]["usertype"].ToString() == "Tester")
                    {
                        //opens tester page when logged inn by tester
                        Tester t = new Tester();
                        t.lblName.Text = "Username: " + txtuserid.Text;
                        holdvalues.UserID = Convert.ToInt32(dt.Rows[0]["userid"]);
                        holdvalues.UserName = dt.Rows[0]["username"].ToString();
                        t.Show();
                        this.Hide();
                    }

                    else if (dt.Rows[0]["usertype"].ToString() == "Developer")
                    {
                        //opens developer page when logged in by developer
                        Developer t = new Developer();
                        t.lblName.Text = "Username: " + txtuserid.Text;
                        holdvalues.UserID = Convert.ToInt32(dt.Rows[0]["userid"]);
                        holdvalues.UserName = dt.Rows[0]["username"].ToString();
                        t.Show();
                        this.Hide();

                    }

                }
                else
                {
                    MessageBox.Show("Username or password wrong");
                }
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

      
    }
}
